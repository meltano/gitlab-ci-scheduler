#!/usr/bin/env python
from setuptools import setup, find_packages

setup(
    name="gitlab-ci-scheduler",
    version="0.0.1",
    author="Meltano Team & Contributors",
    author_email="hello@meltano.com",
    description="Extremely extensible scheduler built on top of GitLab CI",
    url="https://gitlab.com/meltano/gitlab-ci-scheduler",
    py_modules=["gitlab_ci_scheduler"],
    packages=find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    install_requires=["Jinja2>=2.11.2", "python-gitlab>=2.2.0", "PyYAML"],
    entry_points={
        "console_scripts": ["gitlab-ci-scheduler = gitlab_ci_scheduler:main"]
    },
)
