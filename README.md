# gitlab-ci-scheduler

Extremely extensible scheduler built on top of GitLab CI

## Usage

1. Copy the `example` folder and `.gitlab-ci.yml` file??????
1. Generate a personal access token
1. Add a new CI/CD variable with key `GITLAB_API_TOKEN` and the token as value