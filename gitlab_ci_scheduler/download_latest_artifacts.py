#!/usr/bin/env python3

import os
import sys
import pathlib
import shutil
import subprocess
from tempfile import NamedTemporaryFile

import requests
import gitlab


def download_artifact(job, file_path):
    print(f"Downloading artifact '{file_path}'...")

    with NamedTemporaryFile() as temp_file:
        try:
            job.artifact(file_path, streamed=True, action=temp_file.write)
            temp_file.flush()
        except requests.exceptions.RequestException as err:
            print(f"Artifact '{file_path}' could not be found: {err}", file=sys.stderr)
            return False

        path = pathlib.Path(file_path)
        path.parent.mkdir(parents=True, exist_ok=True)

        shutil.copyfile(temp_file.name, file_path)

        print(f"Successfully downloaded artifact '{file_path}'")

        return True


def download_and_extract_artifacts(job, file_paths):
    print(f"Downloading artifacts bundle...")

    with NamedTemporaryFile() as temp_file:
        try:
            job.artifacts(streamed=True, action=temp_file.write)
            temp_file.flush()
        except requests.exceptions.RequestException as err:
            print(f"Artifacts bundle could not be found: {err}", file=sys.stderr)
            return False

        print(f"Successfully downloaded artifacts bundle")

        print(f"Extracting artifacts {file_paths} from bundle...")

        try:
            subprocess.run(["unzip", "-o", temp_file.name, *file_paths], check=True)
        except subprocess.CalledProcessError as err:
            print(f"Artifacts could not be extracted: {err}", file=sys.stderr)
            return False

        print(f"Successfully extracted artifacts {file_paths}")

        return True


def download_artifacts(job, file_paths):
    if any("*" in p for p in file_paths):
        return download_and_extract_artifacts(job, file_paths)
    else:
        return all(download_artifact(job, p) for p in file_paths)


def download_latest_artifacts(project, job_name, file_paths):
    jobs = project.jobs.list(scope="success", as_list=False)
    try:
        latest_job = next((j for j in jobs if j.name == job_name))
    except StopIteration:
        print(
            f"Successful run for job '{job_name}' could not be found", file=sys.stderr
        )
        sys.exit(-1)

    print(f"Found latest successful run for job '{job_name}': Job ID {latest_job.id}")

    if not download_artifacts(latest_job, file_paths):
        sys.exit(-1)
