#!/usr/bin/env python3

import os

import yaml
from jinja2 import Template


def env_var(var, default=None):
    if var in os.environ:
        return os.environ[var]
    elif default is not None:
        return default
    else:
        raise Exception(f"Env var required but not provided: '{var}'")


def env_vars(*vars):
    vars_mapping = {var: env_var(var) for var in vars}

    return yaml.dump(vars_mapping, default_flow_style=True)


def compile_pipeline_template(template_path):
    with open(template_path) as template_file:
        template = Template(template_file.read())

    with open("pipeline.gitlab-ci.yml", "w") as destination_file:
        output = template.render(env_var=env_var, env_vars=env_vars)
        destination_file.write(output)
