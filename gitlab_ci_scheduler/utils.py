import os

import gitlab


def get_project():
    server_url = os.getenv(
        "GITLAB_SERVER_URL", os.getenv("CI_SERVER_URL", "https://gitlab.com/")
    )

    try:
        token = os.environ["GITLAB_API_TOKEN"]
    except KeyError:
        raise Exception("GITLAB_API_TOKEN environment variable must be set")
    gl = gitlab.Gitlab(server_url, private_token=token)

    try:
        project_path = os.environ["CI_PROJECT_PATH"]
    except KeyError:
        raise Exception("CI_PROJECT_PATH environment variable must be set")

    return gl.projects.get(project_path, lazy=True)
