#!/usr/bin/env python3

import os
import yaml
import logging
import sys

import gitlab
from gitlab.v4.objects import ProjectPipelineScheduleVariable


def list_schedule_variables(schedule):
    # schedule.variables.list() is not implemented, so we build it manually
    schedule = schedule.manager.get(schedule.id)
    return [
        ProjectPipelineScheduleVariable(schedule.variables, data)
        for data in schedule.attributes["variables"]
    ]


def update_variables(variables_spec, existing_variables, schedule):
    variables = []
    for variable_spec in variables_spec:
        variable = next(
            (v for v in existing_variables if v.key == variable_spec["key"]), None
        )

        if variable:
            variable.value = variable_spec["value"]
            variable.save()

            print(
                f"Updated variable '{variable.key}' on schedule '{schedule.description}'"
            )
        else:
            variable = schedule.variables.create(variable_spec)

            print(
                f"Created variable '{variable.key}' on schedule '{schedule.description}'"
            )

        variables.append(variable)

    outdated_variables = [s for s in existing_variables if s not in variables]
    for variable in outdated_variables:
        variable.delete()

        print(
            f"Deleted outdated variable '{variable.key}' on schedule '{schedule.description}'"
        )


def update_schedules(schedules_spec, existing_schedules, project):
    schedules = []
    for schedule_spec in schedules_spec:
        schedule = next(
            (
                s
                for s in existing_schedules
                if s.description == schedule_spec["description"]
            ),
            None,
        )

        if schedule:
            schedule.cron = schedule_spec["cron"]
            schedule.ref = schedule_spec["ref"]
            schedule.save()

            existing_variables = list_schedule_variables(schedule)

            print(f"Updated pipeline schedule '{schedule.description}'")
        else:
            schedule = project.pipelineschedules.create(schedule_spec)

            existing_variables = []

            print(f"Created pipeline schedule '{schedule.description}'")

        update_variables(schedule_spec["variables"], existing_variables, schedule)

        schedules.append(schedule)

    outdated_schedules = [s for s in existing_schedules if s not in schedules]
    for schedule in outdated_schedules:
        schedule.delete()

        print(f"Deleted outdated pipeline schedule '{schedule.description}'")


def serialize_variable_value(value):
    if isinstance(value, str):
        return value
    else:
        return yaml.dump(value, default_flow_style=True)


def schedule_pipelines(project, schedule_file_path):
    with open(schedule_file_path) as schedule_file:
        schedule_raw = schedule_file.read()

    schedule = yaml.safe_load(schedule_raw)

    for namespace, schedules in schedule.items():
        schedules_spec = [
            {
                "description": f"{namespace}:{s['name']}",
                "cron": s["cron"],
                "cron_timezone": s.get("cron_timezone", "UTC"),
                "ref": s.get("ref", "master"),
                "variables": [
                    *[
                        {"key": key, "value": serialize_variable_value(value)}
                        for key, value in s.get("variables", {}).items()
                    ],
                    {"key": "CI_SCHEDULE_NAMESPACE", "value": namespace},
                    {"key": "CI_SCHEDULE_NAME", "value": s["name"]},
                    {
                        "key": "CI_SCHEDULE_FULL_NAME",
                        "value": f"{namespace}:{s['name']}",
                    },
                ],
            }
            for s in schedules
        ]

        existing_schedules = [
            s
            for s in project.pipelineschedules.list(as_list=False)
            if s.description.startswith(f"{namespace}:")
        ]

        update_schedules(schedules_spec, existing_schedules, project)
