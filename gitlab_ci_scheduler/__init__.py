#!/usr/bin/env python3

import os
import sys

import gitlab

from .schedule_pipelines import schedule_pipelines
from .compile_pipeline_template import compile_pipeline_template
from .download_latest_artifacts import download_latest_artifacts
from .utils import get_project


def main():
    if len(sys.argv) == 1:
        raise Exception(
            "Subcommand 'schedule-pipelines', 'compile-pipeline-template', or 'download-latest-artifacts' must be specified"
        )

    cmd = sys.argv[1]
    if cmd == "schedule-pipelines":
        project = get_project()

        try:
            schedule_file_path = sys.argv[2]
        except IndexError:
            raise Exception(
                "Schedule file path must be provided in command line argument"
            )

        schedule_pipelines(project, schedule_file_path)
    elif cmd == "compile-pipeline-template":
        try:
            template_path = sys.argv[2]
        except IndexError:
            raise Exception("Template path must be provided in command line argument")

        compile_pipeline_template(template_path)
    elif cmd == "download-latest-artifacts":
        project = get_project()

        try:
            job_name = os.environ["CI_JOB_NAME"]
        except KeyError:
            raise Exception("CI_JOB_NAME environment variable must be set")

        if len(sys.argv) == 1:
            raise Exception("File paths must be provided in command line arguments")

        download_latest_artifacts(project, job_name, file_paths=sys.argv[2:])
    else:
        raise Exception(f"Unknown subcommand '{cmd}'")


if __name__ == "__main__":
    main()
