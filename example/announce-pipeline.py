#!/usr/bin/env python3

import os
import yaml
import logging

try:
    schedule_name = os.environ["CI_SCHEDULE_FULL_NAME"]
except KeyError:
    raise Exception("CI_SCHEDULE_FULL_NAME environment variable must be set")

try:
    announce_verb = os.environ["ANNOUNCE_VERB"]
except KeyError:
    raise Exception("ANNOUNCE_VERB environment variable must be set")

try:
    announce_noun = os.environ["ANNOUNCE_NOUN"]
except KeyError:
    raise Exception("ANNOUNCE_NOUN environment variable must be set")

config = {
    "image": "python:3.6",
    "stages": ["run"],
    "before_script": [
        "pip3 install -e .",
        "gitlab-ci-scheduler download-latest-artifacts DATE || true",
    ],
    schedule_name: {
        "stage": "run",
        "variables": {"ANNOUNCE_VERB": announce_verb},
        "script": [
            f'echo "$ANNOUNCE_VERB, {announce_noun}!"',
            'echo "Last successful run: $(cat DATE 2>/dev/null || echo "Never")"',
            "date > DATE",
            'echo "Current time:        $(cat DATE)"',
        ],
        "artifacts": {"paths": ["DATE"], "when": "always"},
    },
}

print(yaml.dump(config, default_flow_style=False))
